package com.example.jwtdemo.controller;

import com.example.jwtdemo.result.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping("/test")
    public Result test(){
        return Result.build(200,"1212121");
    }
}
