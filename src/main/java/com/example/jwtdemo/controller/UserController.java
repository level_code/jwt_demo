package com.example.jwtdemo.controller;

import com.example.jwtdemo.entity.User;
import com.example.jwtdemo.result.Result;
import com.example.jwtdemo.util.JWTUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@CrossOrigin
public class UserController {
    private final String USERNAME = "admin";
    private final String PASSWORD = "admin";

    @PostMapping("/user/login")
    private Result login(@RequestBody User user){
        JWTUtils jwtConfig = new JWTUtils();
        if (USERNAME.equals(user.getUserName()) && PASSWORD.equals(user.getPassword())){
            HashMap<String, String> map = new HashMap<>();
            map.put(user.getUserName(), user.getPassword());
            user.setToken(jwtConfig.getToken(map));
            return Result.ok(user);
        }else {
            return Result.build(1000,"用户名或密码错误！！");
        }
    }
}
