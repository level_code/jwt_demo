package com.example.jwtdemo.interceptor;
import com.auth0.jwt.exceptions.AlgorithmMismatchException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.example.jwtdemo.result.Result;
import com.example.jwtdemo.result.ResultCodeEnum;
import com.example.jwtdemo.util.JWTUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class TokenInterceptor extends HandlerInterceptorAdapter {

    /**
     * 在这里验证token的正确性，通过返回true，失败返回false
     * @param request  请求
     * @param response 响应
     * @param handler  handler
     * @return 是否通行
     * @throws Exception 异常
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //结果封装
        Result result = new Result();
        //从请求头中获得token
        String token = request.getHeader("token");
        //开始验证
        try {
            JWTUtils.verify(token);
            //没有异常，验证成功，放行
            return true;
        } catch (SignatureVerificationException e) {
            result = Result.build(ResultCodeEnum.PERMISSION.getCode(), "token签名无效");
        } catch (TokenExpiredException e) {
            result = Result.build(ResultCodeEnum.PERMISSION.getCode(), "token过期");
        } catch (AlgorithmMismatchException e) {
            result = Result.build(ResultCodeEnum.PERMISSION.getCode(), "token算法不一致");
        } catch (Exception e) {
            result = Result.build(ResultCodeEnum.PERMISSION.getCode(), "token无效");
        }
        //将结果转成json，并返回给界面
        String resultJson = new ObjectMapper().writeValueAsString(result);
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().println(resultJson);
        //拦截
        return false;
    }
}