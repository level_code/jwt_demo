package com.example.jwtdemo.exception;

import lombok.Data;

@Data
public class ExceptionInfo extends RuntimeException{
    private static final long serialVersionUID = 2870428181462432015L;

    private Integer code;
    private String msg;

    public ExceptionInfo(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
