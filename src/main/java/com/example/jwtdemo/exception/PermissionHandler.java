package com.example.jwtdemo.exception;

import com.example.jwtdemo.result.Result;
import com.example.jwtdemo.result.ResultCodeEnum;
import io.jsonwebtoken.SignatureException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class PermissionHandler {
    @ExceptionHandler(value = { SignatureException.class })
    @ResponseBody
    public Result authorizationException(SignatureException e){
        return Result.build(new ExceptionInfo(1,e.getMessage()), ResultCodeEnum.CODE_ERROR.LOGIN_AUTH);
    }
}